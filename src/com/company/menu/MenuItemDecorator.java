package com.company.menu;

public abstract class MenuItemDecorator extends MenuItem{
    public abstract String getDescription();
}
